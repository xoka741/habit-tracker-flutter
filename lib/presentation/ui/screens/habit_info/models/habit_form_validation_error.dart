import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/form_validation_error.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/validation_error.dart';

class HabitFormValidationError extends FormValidationError {
  final ValidationError? name;
  final ValidationError? description;

  const HabitFormValidationError({
    this.name,
    this.description,
  });

  List<Object?> get props => [name, description];
}
