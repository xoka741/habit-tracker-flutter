import 'package:flutter/material.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_duration.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/app_form.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/form_validation_error.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/validation_error.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/models/habit_form_validation_error.dart';

class HabitForm extends AppForm {
  final name = TextEditingController();
  final description = TextEditingController();
  final type = ValueNotifier<HabitType>(HabitType.good);
  final priority = ValueNotifier<HabitPriority>(HabitPriority.low);
  final duration = ValueNotifier<HabitDuration>(HabitDuration.day);
  final count = TextEditingController();

  late final Habit initialHabit;

  void setData(Habit habit) {
    initialHabit = habit;
    name.text = habit.name;
    description.text = habit.description;
    priority.value = habit.priority;
    type.value = habit.type;
    count.text = habit.count.toString();
    duration.value = HabitDuration.values
        .firstWhere((e) => e.daysNumber == habit.duration.inDays);
  }

  Habit get habit => Habit(
        id: initialHabit.id,
        name: name.text,
        description: description.text,
        priority: priority.value,
        type: type.value,
        count: int.tryParse(count.text) ?? 0,
        duration: Duration(days: duration.value.daysNumber),
        creationDate: initialHabit.creationDate,
        isSynced: initialHabit.isSynced,
        doneDates: initialHabit.doneDates,
      );

  @override
  FormValidationError? validate() {
    if (name.text.isEmpty) {
      return const HabitFormValidationError(
        name: ValidationError.required,
      );
    }

    if (description.text.isEmpty) {
      return const HabitFormValidationError(
        description: ValidationError.required,
      );
    }

    return null;
  }

  @override
  void dispose() {
    name.dispose();
    description.dispose();
    type.dispose();
    priority.dispose();
    count.dispose();
    duration.dispose();
    super.dispose();
  }
}
