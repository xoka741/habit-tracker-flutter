import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/di/injection.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/bloc/create/habit_create_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/bloc/update/habit_update_cubit.dart';

class HabitInfoDependencies extends StatelessWidget {
  final Widget child;
  final String? habitId;

  const HabitInfoDependencies({
    super.key,
    required this.child,
    required this.habitId,
  });

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          lazy: true,
          create: (_) => locator.get<HabitUpdateCubit>(param1: habitId),
        ),
        BlocProvider(
          lazy: true,
          create: (_) => locator.get<HabitCreateCubit>(),
        ),
      ],
      child: child,
    );
  }
}
