import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_duration.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';
import 'package:habit_tracker_flutter/presentation/ui/components/listenable_dropdown_button.dart';
import 'package:habit_tracker_flutter/presentation/ui/components/listenable_radio_list.dart';
import 'package:habit_tracker_flutter/presentation/ui/components/loading_screen.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/form_screen/form_screen_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/form_screen/form_screen_state.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/bloc/create/habit_create_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/bloc/update/habit_update_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/dependencies/habit_info_dependencies.dart';

import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/models/habit_form.dart';

@RoutePage()
class HabitInfoScreen extends StatelessWidget implements AutoRouteWrapper {
  final String? habitId;

  const HabitInfoScreen({super.key, required this.habitId});

  @override
  Widget wrappedRoute(BuildContext context) {
    return HabitInfoDependencies(
      habitId: habitId,
      child: this,
    );
  }

  FormScreenCubit<HabitForm> getCubit(BuildContext context) => habitId != null
      ? context.read<HabitUpdateCubit>()
      : context.read<HabitCreateCubit>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(habitId == null ? 'Create' : 'Update')),
      floatingActionButton: FloatingActionButton(
        onPressed: getCubit(context).submitForm,
        child: const Icon(Icons.check),
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: BlocConsumer(
          bloc: getCubit(context),
          listener: (context, state) {
            if (state is FormSubmittedState<Habit>) {
              context.router.back();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('Saved habit "${state.data?.name}" '),
                  behavior: SnackBarBehavior.floating,
                ),
              );
            }
          },
          builder: (context, state) {
            if (state is FormPreparationState || state is FormSubmittingState) {
              return const LoadingScreen();
            }

            final form = getCubit(context).form;
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                TextField(
                  controller: form.name,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(20),
                  ],
                  decoration: InputDecoration(
                    labelText: 'Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: const BorderSide(),
                    ),
                    hintText: 'Enter text...',
                  ),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: form.description,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(128),
                  ],
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    labelText: 'Description',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: const BorderSide(),
                    ),
                    hintText: 'Enter text...',
                  ),
                ),
                const SizedBox(height: 10),
                const Text('Select type:'),
                Expanded(
                  child: ListenableRadioList(
                    items: HabitType.values,
                    controller: form.type,
                  ),
                ),
                const SizedBox(height: 10),
                const Text('Select priority:'),
                Expanded(
                  child: ListenableRadioList(
                    items: HabitPriority.values,
                    controller: form.priority,
                  ),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: form.count,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(5),
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Count',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: const BorderSide(),
                    ),
                    hintText: 'Enter number...',
                  ),
                ),
                const SizedBox(height: 10),
                ListenableDropdownButton(
                  controller: form.duration,
                  items: HabitDuration.values,
                ),
                const SizedBox(height: 10),
              ],
            );
          },
        ),
      ),
    );
  }
}
