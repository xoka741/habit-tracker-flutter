import 'package:habit_tracker_flutter/data/models/error.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/repositories/habits_repository.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/form_screen/form_screen_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/form_screen/form_screen_state.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_info/models/habit_form.dart';
import 'package:injectable/injectable.dart';

@injectable
class HabitCreateCubit extends FormScreenCubit<HabitForm> {
  final HabitsRepository _repository;

  HabitCreateCubit(this._repository) : super(HabitForm());

  @override
  Future<void> prepareData() async {
    emit(FormPreparationState(form));
    form.setData(Habit.getDefault());
    emit(FormEditingState(form));
  }

  @override
  Future<void> sendForm() async {
    try {
      await _repository.update(form.habit);
      emit(FormSubmittedState(form, data: form.habit));
    } on ServerError {
      emit(FormSubmittedState(form, data: form.habit));
    }
  }
}
