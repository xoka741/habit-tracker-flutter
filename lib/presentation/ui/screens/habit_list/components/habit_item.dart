import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_interaction_cubit.dart';
import 'package:habit_tracker_flutter/routing/app_router.dart';

class HabitItem extends StatelessWidget {
  final Habit habit;

  const HabitItem({super.key, required this.habit});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => context.router.navigate(HabitInfoRoute(habitId: habit.id)),
      leading: Icon(
        habit.type == HabitType.good ? Icons.mood : Icons.mood_bad,
        color: habit.type == HabitType.good ? Colors.green : Colors.red,
      ),
      title: Text(habit.name),
      subtitle: Text(habit.description.characters.take(40).string),
      trailing: IconButton(
        color: Colors.blue,
        iconSize: 22,
        icon: const Icon(Icons.check),
        onPressed: () {
          context.read<HabitInteractionCubit>().completeHabit(habit);
        },
      ),
    );
  }
}
