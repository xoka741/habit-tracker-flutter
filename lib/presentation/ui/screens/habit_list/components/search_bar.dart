import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_list_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/components/filters_bottom_sheet.dart';

class HabitsSearchBar extends StatelessWidget {
  const HabitsSearchBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Row(
        children: [
          Expanded(
            child: BlocBuilder<HabitListCubit, HabitListState>(
              builder: (context, state) {
                return TextField(
                  controller:
                      context.read<HabitListCubit>().habitFilter.searchQuery,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.all(0),
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.black,
                      size: 20,
                    ),
                    prefixIconConstraints:
                        BoxConstraints(maxHeight: 20, minWidth: 25),
                    border: InputBorder.none,
                    hintText: 'Search',
                  ),
                );
              },
            ),
          ),
          IconButton(
            onPressed: () {
              showModalBottomSheet(
                context: context,
                builder: (_) => BlocProvider.value(
                  value: BlocProvider.of<HabitListCubit>(context),
                  child: const FiltersBottomSheet(),
                ),
              );
            },
            icon: const Icon(Icons.filter_alt_outlined),
          ),
        ],
      ),
    );
  }
}
