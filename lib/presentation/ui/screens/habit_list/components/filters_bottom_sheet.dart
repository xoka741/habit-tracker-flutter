import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';
import 'package:habit_tracker_flutter/presentation/ui/components/listenable_checkbox_list.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_list_cubit.dart';

class FiltersBottomSheet extends StatelessWidget {
  const FiltersBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      child: Wrap(
        children: [
          const Text('Select priority'),
          ListenableCheckBoxList(
            controller:
                context.read<HabitListCubit>().habitFilter.priorityFilters,
            items: HabitPriority.values,
          ),
          const Text('Select type'),
          ListenableCheckBoxList(
            controller: context.read<HabitListCubit>().habitFilter.typeFilters,
            items: HabitType.values,
          ),
        ],
      ),
    );
  }
}
