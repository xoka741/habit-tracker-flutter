import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';

class HabitFilter extends ChangeNotifier {
  final typeFilters =
      ValueNotifier<HashSet<HabitType>>(HashSet.from(HabitType.values));
  final priorityFilters =
      ValueNotifier<HashSet<HabitPriority>>(HashSet.from(HabitPriority.values));
  final searchQuery = TextEditingController();

  bool isFiltered(Habit habit) {
    final filteredByName =
        habit.name.toLowerCase().contains(searchQuery.text.toLowerCase());
    final filteredByType = typeFilters.value.contains(habit.type);
    final filteredByPriority = priorityFilters.value.contains(habit.priority);

    return filteredByName && filteredByType && filteredByPriority;
  }

  @override
  void addListener(VoidCallback listener) {
    typeFilters.addListener(listener);
    priorityFilters.addListener(listener);
    searchQuery.addListener(listener);
  }

  @override
  void removeListener(VoidCallback listener) {
    typeFilters.removeListener(listener);
    priorityFilters.removeListener(listener);
    searchQuery.removeListener(listener);
  }

  @override
  void dispose() {
    typeFilters.dispose();
    priorityFilters.dispose();
    searchQuery.dispose();
    super.dispose();
  }
}
