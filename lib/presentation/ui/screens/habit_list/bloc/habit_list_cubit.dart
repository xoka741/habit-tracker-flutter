import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/repositories/habits_repository.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/filtering/habit_filter.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

part 'habit_list_state.dart';

@injectable
class HabitListCubit extends Cubit<HabitListState> {
  final HabitsRepository _repository;

  final habitFilter = HabitFilter();

  late final StreamSubscription<HabitListState> _subscription;

  final _filterController = BehaviorSubject<HabitFilter>()..add(HabitFilter());

  HabitListCubit(this._repository) : super(HabitListLoadingState()) {
    habitFilter.addListener(_updateFilter);

    _subscription = Rx.combineLatest2(
      _repository.getAll(),
      _filterController.stream,
      (habits, filter) => HabitListLoadedState(
        habits.where((e) => filter.isFiltered(e)).toList(),
      ),
    ).listen((state) => emit(state));
  }

  void _updateFilter() {
    _filterController.add(habitFilter);
  }

  @override
  Future<void> close() async {
    habitFilter.dispose();
    habitFilter.removeListener(_updateFilter);
    await _filterController.close();
    await _subscription.cancel();
    return super.close();
  }
}
