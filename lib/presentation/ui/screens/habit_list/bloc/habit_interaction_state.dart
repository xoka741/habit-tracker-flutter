part of 'habit_interaction_cubit.dart';

@immutable
sealed class HabitInteractionState {}

class HabitInteractionInitial extends HabitInteractionState {}

class HabitCompletedState extends HabitInteractionState {
  final Habit habit;

  HabitCompletedState(this.habit);
}
