import 'package:bloc/bloc.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/repositories/habits_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'habit_interaction_state.dart';

@injectable
class HabitInteractionCubit extends Cubit<HabitInteractionState> {
  final HabitsRepository _repository;

  HabitInteractionCubit(this._repository) : super(HabitInteractionInitial());

  Future<void> completeHabit(Habit habit) async {
    try {
      await _repository.completeHabit(habit);
    } catch (_) {}

    emit(HabitCompletedState(habit.copyWith(count: habit.count + 1)));
  }
}
