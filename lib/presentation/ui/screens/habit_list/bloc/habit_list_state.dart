part of 'habit_list_cubit.dart';

sealed class HabitListState {}

class HabitListLoadingState extends HabitListState {}

class HabitListLoadedState extends HabitListState {
  final List<Habit> habits;

  HabitListLoadedState(this.habits);
}
