import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/di/injection.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_interaction_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_list_cubit.dart';

class HabitListDependencies extends StatelessWidget {
  final Widget child;

  const HabitListDependencies({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => locator<HabitInteractionCubit>(),
        ),
        BlocProvider(
          create: (_) => locator<HabitListCubit>(),
        ),
      ],
      child: child,
    );
  }
}
