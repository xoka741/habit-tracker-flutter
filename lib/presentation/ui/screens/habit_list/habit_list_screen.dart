import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:habit_tracker_flutter/presentation/ui/components/loading_screen.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_interaction_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/bloc/habit_list_cubit.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/components/habit_item.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/components/search_bar.dart';
import 'package:habit_tracker_flutter/presentation/ui/screens/habit_list/dependencies/habit_list_dependencies.dart';
import 'package:habit_tracker_flutter/routing/app_router.dart';

@RoutePage()
class HabitListScreen extends StatelessWidget implements AutoRouteWrapper {
  const HabitListScreen({super.key});

  @override
  Widget wrappedRoute(BuildContext context) =>
      HabitListDependencies(child: this);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Habits')),
      body: BlocListener<HabitInteractionCubit, HabitInteractionState>(
        listener: (context, state) {
          if (state is HabitCompletedState) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text('${state.habit.name} has been completed')),
            );
          }
        },
        child: BlocBuilder<HabitListCubit, HabitListState>(
          builder: (context, state) => switch (state) {
            HabitListLoadingState() => const LoadingScreen(),
            HabitListLoadedState() => Column(
                children: [
                  const HabitsSearchBar(),
                  Expanded(
                    child: ListView.builder(
                      itemCount: state.habits.length,
                      itemBuilder: (context, index) =>
                          HabitItem(habit: state.habits[index]),
                    ),
                  ),
                ],
              ),
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.router.navigate(HabitInfoRoute(habitId: null)),
        child: const Icon(Icons.add),
      ),
    );
  }
}
