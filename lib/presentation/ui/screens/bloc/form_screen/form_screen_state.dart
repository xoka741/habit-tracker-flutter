import 'package:equatable/equatable.dart';

import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/app_form.dart';

import 'package:habit_tracker_flutter/presentation/ui/screens/bloc/models/form/form_validation_error.dart';

sealed class FormScreenState extends Equatable {
  final AppForm form;

  const FormScreenState(this.form);

  @override
  List<Object?> get props => [form];
}

abstract interface class EditableState {}

// Preparation
final class FormPreparationState extends FormScreenState {
  const FormPreparationState(super.form);
}

final class FormPreparationFailedState extends FormScreenState {
  final Object? error;

  const FormPreparationFailedState(super.form, this.error);

  @override
  List<Object?> get props => [form, error];
}

// Editing
final class FormEditingState extends FormScreenState implements EditableState {
  const FormEditingState(super.form);
}

// Validation
final class FormValidationFailedState extends FormScreenState implements EditableState {
  final FormValidationError validationError;

  const FormValidationFailedState(super.form, this.validationError);

  @override
  List<Object?> get props => [form, validationError];
}

// Confirmation
final class FormNeedsConfirmationState extends FormScreenState {
  const FormNeedsConfirmationState(super.form);
}

// Submitting
final class FormSubmittingState extends FormScreenState implements EditableState {
  const FormSubmittingState(super.form);
}

final class FormSubmittedState<T> extends FormScreenState implements EditableState {
  final T? data;

  const FormSubmittedState(super.form, {this.data});

  @override
  List<Object?> get props => [form, data];
}

final class FormSubmissionFailedState extends FormScreenState implements EditableState {
  final Object? error;

  const FormSubmissionFailedState(super.form, this.error);

  @override
  List<Object?> get props => [form, error];
}
