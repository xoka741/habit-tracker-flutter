enum ValidationError {
  invalid,
  required,
  tooLong,
  tooShort;

  String? toErrorString({
    required String? invalidError,
    required String? requiredError,
    String? tooLongError,
    String? tooShortError,
  }) {
    switch (this) {
      case ValidationError.invalid:
        return invalidError;
      case ValidationError.required:
        return requiredError;
      case ValidationError.tooLong:
        return tooLongError ?? invalidError;
      case ValidationError.tooShort:
        return tooShortError ?? invalidError;
    }
  }
}
