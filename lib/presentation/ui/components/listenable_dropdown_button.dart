import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';

class ListenableDropdownButton<T extends Enum> extends StatelessWidget {
  const ListenableDropdownButton({
    super.key,
    required this.controller,
    required this.items,
  });

  final ValueNotifier<T> controller;
  final List<T> items;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: controller,
      builder: (context, value, widget) {
        return DropdownButton<T>(
          borderRadius: BorderRadius.circular(25.0),
          value: value,
          icon: const Icon(Icons.keyboard_arrow_down),
          onChanged: (value) {
            if (value != null) {
              controller.value = value;
            }
          },
          items: items
              .map(
                (T value) => DropdownMenuItem<T>(
                  value: value,
                  child: Text(
                    StringUtils.capitalize(value.name),
                    style: const TextStyle(fontSize: 30),
                  ),
                ),
              )
              .toList(),
        );
      },
    );
  }
}
