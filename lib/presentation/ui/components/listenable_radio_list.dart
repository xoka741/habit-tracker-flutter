import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';

class ListenableRadioList<T extends Enum> extends StatelessWidget {
  const ListenableRadioList({
    super.key,
    required this.items,
    required this.controller,
  });

  final List<T> items;
  final ValueNotifier<T> controller;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: controller,
      builder: (context, value, child) => ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) => RadioListTile<T>(
          title: Text(StringUtils.capitalize(items[index].name)),
          groupValue: value,
          value: items[index],
          onChanged: (T? value) {
            if (value != null) {
              controller.value = value;
            }
          },
        ),
      ),
    );
  }
}
