import 'dart:collection';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';

class ListenableCheckBoxList<T extends Enum> extends StatelessWidget {
  const ListenableCheckBoxList({
    super.key,
    required this.controller,
    required this.items,
  });

  final List<T> items;
  final ValueNotifier<HashSet<T>> controller;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: controller,
      builder: (BuildContext context, HashSet<T> value, Widget? child) {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: items.length,
          itemBuilder: (context, index) {
            final item = items[index];
            return ListTile(
              title: Text(StringUtils.capitalize(item.name)),
              leading: Checkbox(
                onChanged: (bool? value) {
                  if (value != null) {
                    if (value) {
                      controller.value = HashSet.from(controller.value)
                        ..add(item);
                    } else {
                      controller.value = HashSet.from(controller.value)
                        ..remove(item);
                    }
                  }
                },
                value: value.contains(item),
              ),
            );
          },
        );
      },
    );
  }
}
