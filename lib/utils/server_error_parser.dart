import 'package:dio/dio.dart';
import 'package:habit_tracker_flutter/data/models/error.dart';

mixin ServerErrorParserMixin {
  Future<T> handleError<T>(Future<T> Function() action) async {
    try {
      return await action();
    } on DioException catch (err) {
      final error = err.error;
      if (error is ServerError) {
        throw error;
      }
      try {
        throw ServerError.fromJson(err.response?.data);
      } on FormatException catch (_) {
        throw err;
      }
    }
  }
}
