import 'package:flutter/material.dart';
import 'package:habit_tracker_flutter/data/data_source/habit_hive.dart';
import 'package:habit_tracker_flutter/di/injection.dart';
import 'package:habit_tracker_flutter/presentation/habit_tracker_app.dart';
import 'package:hive_flutter/hive_flutter.dart';

Future<void> startUp() async {
  await Hive.initFlutter();
  Hive.registerAdapter(HabitHiveAdapter());
}

Future<void> main() async {
  await startUp();
  await configureDependencies();
  runApp(const HabitTrackerApp());
}
