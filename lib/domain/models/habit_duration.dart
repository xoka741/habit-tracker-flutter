enum HabitDuration {
  hour(1),
  day(24),
  week(24 * 7),
  month(24 * 30),
  year(24 * 365);

  const HabitDuration(this.daysNumber);

  final int daysNumber;
}
