import 'package:habit_tracker_flutter/domain/models/habit.dart';

abstract class HabitsRepository {
  Stream<List<Habit>> getAll();

  Future<void> add(Habit habit);

  Future<void> delete(Habit habit);

  Future<void> update(Habit habit);

  Future<Habit?> getById(String id);

  Future<void> completeHabit(Habit habit);
}
