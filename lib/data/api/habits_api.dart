import 'package:dio/dio.dart';
import 'package:habit_tracker_flutter/data/models/habit_done.dart';
import 'package:habit_tracker_flutter/data/models/habit_dto.dart';
import 'package:habit_tracker_flutter/data/models/habit_uid.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'habits_api.g.dart';

@lazySingleton
@RestApi()
abstract class HabitsApi {

  @factoryMethod
  factory HabitsApi(Dio dio) = _HabitsApi;

  @GET('/habit')
  Future<List<HabitDto>> getHabits();

  @PUT('/habit')
  Future<HabitUID> addOrUpdateHabit(@Body() HabitDto habit);

  @DELETE('/habit')
  Future<void> deleteHabit(@Body() HabitUID habitUID);

  @POST('/habit_done')
  Future<void> doneHabit(@Body() HabitDone habitDone);
}
