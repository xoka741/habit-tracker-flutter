import 'dart:async';

import 'package:habit_tracker_flutter/data/data_source/habit_hive.dart';
import 'package:habit_tracker_flutter/data/data_source/mappers.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@singleton
class HabitsDataSource {
  final Box<HabitHive> _habitBox;

  final _habitsController = BehaviorSubject<List<Habit>>();

  late final StreamSubscription<BoxEvent> _subscription;

  HabitsDataSource(this._habitBox) {
    _subscription = _habitBox.watch().listen((event) {
      _updateItems();
    });
    _updateItems();
  }

  void _updateItems() {
    _habitsController
        .add(_habitBox.values.map((e) => e.toDomainModel()).toList());
  }

  Stream<List<Habit>> getAll() => _habitsController.stream;

  Future<Habit> addWithGeneratedId(Habit habit) async {
    final id = DateTime.now().toString();
    final savedHabit = habit.copyWith(id: id);
    await addWithProvidedId(savedHabit);
    return savedHabit;
  }

  Future<void> addWithProvidedId(Habit habit) =>
      _habitBox.put(habit.id, habit.toHiveModel());

  Future<void> syncWithId(String oldId, Habit newHabit) async {
    if (_habitBox.containsKey(oldId)) {
      await _habitBox.delete(oldId);
      await addWithProvidedId(newHabit.copyWith(isSynced: true));
    }
  }

  Future<void> delete(Habit habit) => _habitBox.delete(habit.id);

  Future<void> update(Habit habit) =>
      _habitBox.put(habit.id, habit.toHiveModel());

  Future<Habit?> getById(String id) async => _habitBox.get(id)?.toDomainModel();

  @disposeMethod
  Future<void> dispose() => _subscription.cancel();
}
