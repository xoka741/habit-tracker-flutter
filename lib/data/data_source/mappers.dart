import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';

import 'package:habit_tracker_flutter/data/data_source/habit_hive.dart';

extension HabitHiveExtension on HabitHive {
  Habit toDomainModel() {
    return Habit(
      id: id,
      name: name,
      description: description,
      priority: HabitPriority.values[priority],
      type: HabitType.values[type],
      count: count,
      creationDate: creationDate,
      isSynced: isSynced,
      doneDates: doneDates,
      duration: Duration(milliseconds: duration),
    );
  }
}

extension HabitExtension on Habit {
  HabitHive toHiveModel() {
    return HabitHive(
      id: id,
      name: name,
      description: description,
      priority: priority.index,
      type: type.index,
      count: count,
      creationDate: creationDate,
      isSynced: isSynced,
      doneDates: doneDates,
      duration: duration.inMilliseconds,
    );
  }
}
