import 'package:habit_tracker_flutter/data/api/habits_api.dart';
import 'package:habit_tracker_flutter/data/data_source/habits_data_source.dart';
import 'package:habit_tracker_flutter/data/models/habit_done.dart';
import 'package:habit_tracker_flutter/data/models/habit_uid.dart';
import 'package:habit_tracker_flutter/data/models/mappers.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/repositories/habits_repository.dart';
import 'package:habit_tracker_flutter/utils/server_error_parser.dart';
import 'package:injectable/injectable.dart';
import 'package:rxdart/rxdart.dart';

@Injectable(as: HabitsRepository)
class HabitsRepositoryImpl extends HabitsRepository
    with ServerErrorParserMixin {
  final HabitsDataSource _dataSource;
  final HabitsApi _habitsApi;

  HabitsRepositoryImpl(this._dataSource, this._habitsApi);

  @override
  Future<void> add(Habit habit) => _dataSource.addWithGeneratedId(habit);

  @override
  Future<void> delete(Habit habit) async {
    await _dataSource.delete(habit);
    await _habitsApi.deleteHabit(HabitUID(uid: habit.id));
  }

  @override
  Future<void> completeHabit(Habit habit) async {
    final newHabit = habit.copyWith(count: habit.count + 1);
    await _dataSource.update(newHabit);
    await _withHabitSync(newHabit, (habit) async {
      await _habitsApi.doneHabit(
        HabitDone(
          uid: habit.id,
          date: DateTime.now().millisecondsSinceEpoch,
        ),
      );
    });
  }

  @override
  Stream<List<Habit>> getAll() => _dataSource.getAll().doOnData((data) async {
        for (final habit in data) {
          await _withHabitSync(habit, (habit) => Future.value(null));
        }
      });

  @override
  Future<Habit?> getById(String id) => _dataSource.getById(id);

  @override
  Future<void> update(Habit habit) async {
    final newHabit = habit.copyWith(creationDate: DateTime.now());
    await _dataSource.update(newHabit);
  }

  Future<T> _withHabitSync<T>(
    Habit habit,
    Future<T> Function(Habit habit) action,
  ) =>
      handleError(() async {
        if (!habit.isSynced) {
          final dto = habit.copyWith(id: '').toDto();
          final response = await _habitsApi.addOrUpdateHabit(dto);
          final newHabit = habit.copyWith(id: response.uid);
          await _dataSource.syncWithId(habit.id, newHabit);
          return await action(habit.copyWith(id: response.uid));
        }
        return await action(habit);
      });
}
