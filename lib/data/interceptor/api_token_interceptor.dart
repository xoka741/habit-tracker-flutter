import 'package:dio/dio.dart';
import 'package:habit_tracker_flutter/di/constants/injection_keys.dart';
import 'package:injectable/injectable.dart';


@injectable
class ApiTokenInterceptor extends Interceptor {
  final String apiToken;

  ApiTokenInterceptor(@Named(InjectionKeys.apiToken) this.apiToken);

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers.addAll(<String, String>{'Authorization': apiToken});
    super.onRequest(options, handler);
  }
}
