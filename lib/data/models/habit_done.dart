import 'package:json_annotation/json_annotation.dart';

part 'habit_done.g.dart';

@JsonSerializable()
class HabitDone {
  @JsonKey(name: 'habit_uid')
  final String uid;
  final int date;

  factory HabitDone.fromJson(Map<String, dynamic> json) =>
      _$HabitDoneFromJson(json);

  HabitDone({required this.uid, required this.date});

  Map<String, dynamic> toJson() => _$HabitDoneToJson(this);
}
