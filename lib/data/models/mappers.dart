import 'package:habit_tracker_flutter/data/models/habit_dto.dart';
import 'package:habit_tracker_flutter/domain/models/habit.dart';
import 'package:habit_tracker_flutter/domain/models/habit_priority.dart';
import 'package:habit_tracker_flutter/domain/models/habit_type.dart';

extension HabitDtoExtension on HabitDto {
  Habit toDomainModel() {
    return Habit(
      id: uid,
      name: title,
      description: description,
      priority: HabitPriority.values[priority],
      type: HabitType.values[type],
      count: count,
      creationDate: DateTime.fromMillisecondsSinceEpoch(date),
      isSynced: false,
      doneDates: doneDates,
      duration: Duration(hours: frequency),
    );
  }
}

extension HabitExtension on Habit {
  HabitDto toDto() {
    return HabitDto(
      color: 0,
      uid: id,
      title: name,
      description: description,
      priority: priority.index,
      type: type.index,
      count: count,
      doneDates: doneDates,
      date: creationDate.millisecondsSinceEpoch,
      frequency: duration.inHours,
    );
  }
}
