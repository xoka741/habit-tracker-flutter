import 'package:json_annotation/json_annotation.dart';

part 'habit_uid.g.dart';

@JsonSerializable()
class HabitUID {
  final String uid;

  HabitUID({required this.uid});

  factory HabitUID.fromJson(Map<String, dynamic> json) =>
      _$HabitUIDFromJson(json);

  Map<String, dynamic> toJson() => _$HabitUIDToJson(this);
}
