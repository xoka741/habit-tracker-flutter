import 'package:json_annotation/json_annotation.dart';

part 'habit_dto.g.dart';

@JsonSerializable()
class HabitDto {
  final String uid;
  final String title;
  final String description;
  final int priority;
  final int type;
  final int count;
  final int date;
  final int color;

  HabitDto({
    required this.uid,
    required this.title,
    required this.description,
    required this.priority,
    required this.type,
    required this.count,
    required this.date,
    required this.color,
    required this.doneDates,
    required this.frequency,
  });

  @JsonKey(name: 'done_dates')
  final List<int> doneDates;
  final int frequency;

  factory HabitDto.fromJson(Map<String, dynamic> json) =>
      _$HabitDtoFromJson(json);

  Map<String, dynamic> toJson() => _$HabitDtoToJson(this);
}
