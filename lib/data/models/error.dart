import 'package:json_annotation/json_annotation.dart';

part 'error.g.dart';

@JsonSerializable(createToJson: false)
class ServerError {
  final int code;
  final String message;

  ServerError(this.code, this.message);

  factory ServerError.fromJson(Map<String, dynamic> json) =>
      _$ServerErrorFromJson(json);
}
