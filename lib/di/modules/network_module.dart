import 'package:dio/dio.dart';
import 'package:habit_tracker_flutter/data/interceptor/api_token_interceptor.dart';
import 'package:habit_tracker_flutter/di/constants/environment_configuration.dart';
import 'package:habit_tracker_flutter/di/constants/injection_keys.dart';
import 'package:injectable/injectable.dart';

@module
abstract class NetworkModule {
  @Named(InjectionKeys.baseUrl)
  String get baseUrl => EnvironmentConfiguration.baseUrl;

  @Named(InjectionKeys.apiToken)
  String get apiToken => EnvironmentConfiguration.apiToken;

  Dio dio(
    ApiTokenInterceptor apiTokenInterceptor,
    @Named(InjectionKeys.baseUrl) String url,
  ) =>
      Dio(
        BaseOptions(
          baseUrl: url,
          listFormat: ListFormat.multi,
        ),
      )..interceptors.addAll([
          apiTokenInterceptor,
        ]);
}
