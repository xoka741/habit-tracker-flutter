import 'package:habit_tracker_flutter/data/data_source/habit_hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';

@module
abstract class DataModule {
  @singleton
  @preResolve
  Future<Box<HabitHive>> getHabitBox() => Hive.openBox<HabitHive>('habits');
}
