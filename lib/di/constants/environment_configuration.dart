import 'package:habit_tracker_flutter/di/constants/configuration_keys.dart';

class EnvironmentConfiguration {
  static const baseUrl = String.fromEnvironment(ConfigurationKeys.baseUrl);
  static const apiToken = String.fromEnvironment(ConfigurationKeys.apiToken);
}
