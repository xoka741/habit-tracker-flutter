import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:habit_tracker_flutter/di/injection.config.dart';

final locator = GetIt.instance;

@InjectableInit(preferRelativeImports: false)
Future<void> configureDependencies() async => await locator.init();
